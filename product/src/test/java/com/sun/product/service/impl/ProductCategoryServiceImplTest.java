package com.sun.product.service.impl;


import com.sun.product.ProductApplicationTests;
import com.sun.product.dataobject.ProductCategory;
import com.sun.product.service.ProductCategoryService;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductCategoryServiceImplTest extends ProductApplicationTests {

  @Autowired
  private ProductCategoryService productCategoryService;

  @Test
  public void findByCategoryTypeIn() {
    List<ProductCategory> productCategoryList = productCategoryService.findByCategoryTypeIn(
        Arrays.asList(11,22));
    Assert.assertTrue(productCategoryList.size() > 0);
  }
}