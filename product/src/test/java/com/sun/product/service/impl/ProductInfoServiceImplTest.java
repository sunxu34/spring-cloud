package com.sun.product.service.impl;


import com.sun.product.DTO.CartDTO;
import com.sun.product.ProductApplicationTests;
import com.sun.product.dataobject.ProductInfo;
import com.sun.product.service.ProductInfoService;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductInfoServiceImplTest extends ProductApplicationTests {

  @Autowired
  private ProductInfoService productInfoService;

  @Test
  public void findUpAll() {
    List<ProductInfo> productInfoList = productInfoService.findUpAll();
    Assert.assertTrue(productInfoList.size() > 0);
  }

  @Test
  public void findByProducIdList() {
    List<ProductInfo> productInfoList =
        productInfoService.findByProducIdList(Arrays.asList("157875196366160022", "157875227953464068"));
    Assert.assertTrue(productInfoList.size() > 0);
  }

  @Test
  public void decreaseStock() {
    CartDTO cartDTO = new CartDTO("157875196366160022", 2);
    productInfoService.decreaseStock(Arrays.asList(cartDTO));
  }
}