package com.sun.product.repository;


import com.sun.product.ProductApplicationTests;
import com.sun.product.dataobject.ProductInfo;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductInfoRepositoryTest extends ProductApplicationTests {

  @Autowired
  private ProductInfoRepository productInfoRepository;

  @Test
  public void findByProductStatus() {
    List<ProductInfo> productInfoList = productInfoRepository.findByProductStatus(0);
    Assert.assertTrue(productInfoList.size() > 0);
  }

  @Test
  public void findByProductIdIn() {
    List<ProductInfo> productInfoList =
        productInfoRepository.findByProductIdIn(Arrays.asList("157875196366160022", "157875227953464068"));
    Assert.assertTrue(productInfoList.size() > 0);
  }
}