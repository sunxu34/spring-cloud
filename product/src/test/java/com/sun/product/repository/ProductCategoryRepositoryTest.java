package com.sun.product.repository;


import com.sun.product.ProductApplicationTests;
import com.sun.product.dataobject.ProductCategory;
import java.util.Arrays;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class ProductCategoryRepositoryTest extends ProductApplicationTests {

  @Autowired
  private ProductCategoryRepository productCategoryRepository;

  @Test
  public void findByCategoryTypeIn() {
    List<ProductCategory> productCategoryList = productCategoryRepository.findByCategoryTypeIn(
        Arrays.asList(11, 22));
    Assert.assertTrue(productCategoryList.size() > 0);
  }
}