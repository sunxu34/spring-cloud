package com.sun.product.dataobject;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * 商品类目实体
 */
@Data
@Entity
@Table(name = "product_category")
public class ProductCategory {

  /**
   * 类目id
   */
  @Id
  @GeneratedValue
  private Integer categoryId;

  /**
   * 类目名称
   */
  private String categoryName;

  /**
   * 类目编号
   */
  private Integer categoryType;

  /**
   * 创建时间
   */
  private Date createTime;

  /**
   * 更新时间
   */
  private Date updateTime;
}
