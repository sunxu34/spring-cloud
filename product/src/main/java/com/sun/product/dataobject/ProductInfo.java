package com.sun.product.dataobject;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * 商品信息表
 */
@Data
@Entity
@Table(name = "product_info")
public class ProductInfo {

  /**
   * 商品id
   */
  @Id
  private String productId;

  /**
   * 商品名称
   */
  private String productName;

  /**
   * 商品单价
   */
  private BigDecimal productPrice;

  /**
   * 商品库存
   */
  private Integer productStock;

  /**
   * 商品描述
   */
  private String productDescription;

  /**
   * 商品小图
   */
  private String productIcon;

  /**
   * 商品状态
   */
  private Integer productStatus;

  /**
   * 商品类目
   */
  private Integer categoryType;

  /**
   * 创建时间
   */
  private Date createTime;

  /**
   * 更新时间
   */
  private Date updateTime;
}
