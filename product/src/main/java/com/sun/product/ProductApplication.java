package com.sun.product;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ProductApplication implements CommandLineRunner {

  public static void main(String[] args) {
    SpringApplication.run(ProductApplication.class, args);
  }

  @Override
  public void run(String... strings) throws Exception {
    System.out.println("=====================商品服务启动完成=====================");
  }
}
