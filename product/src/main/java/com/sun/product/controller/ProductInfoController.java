package com.sun.product.controller;

import com.sun.product.DTO.CartDTO;
import com.sun.product.VO.ProductInfoVO;
import com.sun.product.VO.ProductVO;
import com.sun.product.VO.ResultVO;
import com.sun.product.dataobject.ProductCategory;
import com.sun.product.dataobject.ProductInfo;
import com.sun.product.service.ProductCategoryService;
import com.sun.product.service.ProductInfoService;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
public class ProductInfoController {

  @Autowired
  private ProductInfoService productInfoService;

  @Autowired
  private ProductCategoryService productCategoryService;

  /**
   * 获取在架商品列表
   * @return 返回值
   */
  @GetMapping("/list")
  public ResultVO<ProductVO> list() {
    // 获取所有在架的商品信息
    List<ProductInfo> productInfoList = productInfoService.findUpAll();
    // 获取在架商品的类目
    List<Integer> categoryTypeList = productInfoList.stream()
        .map(ProductInfo::getCategoryType).collect(Collectors.toList());
    // 从数据库查询类目
    List<ProductCategory> productCategoryList =
        productCategoryService.findByCategoryTypeIn(categoryTypeList);
    // 构造返回数据
    List<ProductVO> productVOList = new ArrayList<>();
    for (ProductCategory productCategory : productCategoryList) {
      ProductVO productVO = new ProductVO();
      productVO.setCategoryName(productCategory.getCategoryName());
      productVO.setCategorytype(productCategory.getCategoryType());
      List<ProductInfoVO> productInfoVOList = new ArrayList<>();
      for (ProductInfo productInfo : productInfoList) {
        if (productInfo.getCategoryType().equals(productCategory.getCategoryType())) {
          ProductInfoVO productInfoVO = new ProductInfoVO();
          BeanUtils.copyProperties(productInfo, productInfoVO);
          productInfoVOList.add(productInfoVO);
        }
      }
      productVO.setProductInfoVOList(productInfoVOList);
      productVOList.add(productVO);
    }
    ResultVO resultVO = new ResultVO();
    resultVO.setCode(0);
    resultVO.setMsg("成功");
    resultVO.setData(productVOList);
    return resultVO;
  }

  /**
   * 获取商品列表（为订单服务提供）
   * @param productIdList
   * @return
   */
  @PostMapping("/listForOrder")
  public List<ProductInfo> listForOrder(@RequestBody List<String> productIdList){
    List<ProductInfo> productInfoList = productInfoService.findByProducIdList(productIdList);
    return productInfoList;
  }

  /**
   * 扣库存接口
   * @param cartDTOList
   */
  @PostMapping("decreaseStock")
  public void decreaseStock(@RequestBody List<CartDTO> cartDTOList){
    productInfoService.decreaseStock(cartDTOList);
  }
}
