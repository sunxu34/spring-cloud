package com.sun.product.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.Data;

@Data
public class ProductInfoVO {

  /**
   * 商品id
   */
  @JsonProperty("id")
  private String productId;

  /**
   * 商品名称
   */
  @JsonProperty("name")
  private String productName;

  /**
   * 商品价格
   */
  @JsonProperty("price")
  private BigDecimal productPrice;

  /**
   * 商品描述
   */
  @JsonProperty("description")
  private String productDescription;

  /**
   * 商品小图
   */
  @JsonProperty("icon")
  private String productIcon;

}
