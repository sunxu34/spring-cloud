package com.sun.product.VO;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Data;

@Data
public class ProductVO {

  /**
   * 类目名称
   */
  @JsonProperty("name")
  private String categoryName;

  /**
   * 类目编号
   */
  @JsonProperty("type")
  private Integer categorytype;

  /**
   * 商品详情列表
   */
  @JsonProperty("foods")
  private List<ProductInfoVO> productInfoVOList;
}
