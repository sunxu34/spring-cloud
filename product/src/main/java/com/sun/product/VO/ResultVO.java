package com.sun.product.VO;

import lombok.Data;

/**
 * 请求返回对象
 * @param <T>
 */
@Data
public class ResultVO<T> {

  /**
   * 错误码
   */
  private Integer code;

  /**
   * 错误信息
   */
  private String msg;

  /**
   * 返回数据
   */
  private T data;
}
