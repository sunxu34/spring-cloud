package com.sun.product.enums;

import lombok.Getter;

/**
 * 商品状态枚举类
 */
@Getter
public enum ProductStatusEnum {

  UP(0, "正常"),
  DOWM(1, "下架"),
  ;

  private Integer code;
  private String message;

  ProductStatusEnum(Integer code, String message) {
    this.code = code;
    this.message = message;
  }

}
