package com.sun.product.repository;

import com.sun.product.dataobject.ProductInfo;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 商品信息持久层
 */
@Repository
public interface ProductInfoRepository extends JpaRepository<ProductInfo, String> {

  /**
   * 根据商品状态查询商品信息
   * @param productStatus 商品状态
   * @return 商品信息集合
   */
  List<ProductInfo> findByProductStatus(Integer productStatus);

  /**
   * 根据商品id查询商品信息
   * @param producIdList
   * @return 商品信息集合
   */
  List<ProductInfo> findByProductIdIn(List<String> producIdList);
}
