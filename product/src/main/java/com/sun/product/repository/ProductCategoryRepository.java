package com.sun.product.repository;

import com.sun.product.dataobject.ProductCategory;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {

  /**
   * 根据类目类型查询类目信息
   * @param categoryTypeList 类型列表
   * @return 类目信息列表
   */
  List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);

}
