package com.sun.product.service.impl;

import com.sun.product.dataobject.ProductCategory;
import com.sun.product.repository.ProductCategoryRepository;
import com.sun.product.service.ProductCategoryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 类目服务实现类
 */
@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

  @Autowired
  private ProductCategoryRepository productCategoryRepository;

  @Override
  public List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList) {
    return productCategoryRepository.findByCategoryTypeIn(categoryTypeList);
  }
}
