package com.sun.product.service;

import com.sun.product.DTO.CartDTO;
import com.sun.product.dataobject.ProductInfo;
import java.util.List;

/**
 * 商品服务接口
 */
public interface ProductInfoService {

  /**
   * 查询所有在架商品列表
   *
   * @return list
   */
  List<ProductInfo> findUpAll();

  /**
   * 根据商品id查询商品信息
   * @return 商品信息集合
   */
  List<ProductInfo> findByProducIdList(List<String> productIdList);

  /**
   * 扣库存
   * @param cartDTOList 购物车列表
   */
  void decreaseStock(List<CartDTO> cartDTOList);
}
