package com.sun.product.service;

import com.sun.product.dataobject.ProductCategory;
import java.util.List;

/**
 * 类目服务接口
 */
public interface ProductCategoryService {


  /**
   * 根据类目编号查询商品类目
   *
   * @param categoryTypeList 类目编号集合
   * @return 类目集合
   */
  List<ProductCategory> findByCategoryTypeIn(List<Integer> categoryTypeList);
}
