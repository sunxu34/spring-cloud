package com.sun.product.service.impl;

import com.sun.product.DTO.CartDTO;
import com.sun.product.dataobject.ProductInfo;
import com.sun.product.enums.ProductStatusEnum;
import com.sun.product.enums.ResultEnum;
import com.sun.product.exception.ProductionException;
import com.sun.product.repository.ProductInfoRepository;
import com.sun.product.service.ProductInfoService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商品服务实现类
 */
@Service
public class ProductInfoServiceImpl implements ProductInfoService {

  @Autowired
  private ProductInfoRepository productInfoRepository;

  /**
   * 查询所有在架商品列表
   * @return
   */
  @Override
  public List<ProductInfo> findUpAll(){
    return productInfoRepository.findByProductStatus(ProductStatusEnum.UP.getCode());
  }

  /**
   * 根据商品id查询商品信息
   * @param productIdList 商品id集合
   * @return 商品信息集合
   */
  @Override
  public List<ProductInfo> findByProducIdList(List<String> productIdList) {
    return productInfoRepository.findByProductIdIn(productIdList);
  }

  @Override
  @Transactional
  public void decreaseStock(List<CartDTO> cartDTOList) {
    for (CartDTO cartDTO : cartDTOList) {
      // 校验商品信息
      Optional<ProductInfo> productInfoOptional =
          productInfoRepository.findById(cartDTO.getProductId());
      if (!productInfoOptional.isPresent()) {
        throw new ProductionException(ResultEnum.PRODUCT_NOT_EXIST);
      }

      // 扣库存
      ProductInfo productInfo = productInfoOptional.get();
      Integer result = productInfo.getProductStock() - cartDTO.getProductQuantity();
      if (result < 0) {
        throw new ProductionException(ResultEnum.PRODUCT_STOCK_ERROR);
      }

      productInfo.setProductStock(result);
      productInfoRepository.save(productInfo);

    }
  }
}
