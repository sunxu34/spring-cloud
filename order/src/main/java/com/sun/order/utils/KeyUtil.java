package com.sun.order.utils;

import java.util.Random;

/**
 * 唯一主键生成工具类
 */
public class KeyUtil {

  /**
   * 生成唯一订单主键id
   * 格式： 时间 + 随机数
   * @return 订单主键字符串
   */
  public static synchronized String genUniqueKey(){
    Random random = new Random();
    Integer number = random.nextInt(900000) + 100000;
    return System.currentTimeMillis() + String.valueOf(number);
  }

}
