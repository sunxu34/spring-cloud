package com.sun.order.dataobject;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 * 订单实体
 */
@Data
@Entity
@Table(name = "order_master")
public class OrderMaster {

  /**
   * 订单id
   */
  @Id
  private String orderId;

  /**
   * 买家姓名
   */
  private String buyerName;

  /**
   * 买家手机号
   */
  private String buyerPhone;

  /**
   * 买家地址
   */
  private String buyerAddress;

  /**
   * 买家微信openid
   */
  private String buyerOpenid;

  /**
   * 订单总价
   */
  private BigDecimal orderAmount;

  /**
   * 订单状态
   */
  private Integer orderStatus;

  /**
   * 支付状态
   */
  private Integer payStatus;

  /**
   * 创建时间
   */
  private Date createTime;

  /**
   * 更新时间
   */
  private Date updateTime;
}
