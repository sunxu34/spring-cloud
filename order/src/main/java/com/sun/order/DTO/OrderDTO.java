package com.sun.order.DTO;

import com.sun.order.dataobject.OrderDetail;
import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

@Data
public class OrderDTO {
  /**
   * 订单id
   */
  private String orderId;

  /**
   * 买家姓名
   */
  private String buyerName;

  /**
   * 买家手机号
   */
  private String buyerPhone;

  /**
   * 买家地址
   */
  private String buyerAddress;

  /**
   * 买家微信openid
   */
  private String buyerOpenid;

  /**
   * 订单总价
   */
  private BigDecimal orderAmount;

  /**
   * 订单状态
   */
  private Integer orderStatus;

  /**
   * 支付状态
   */
  private Integer payStatus;


  private List<OrderDetail> orderDetailList;
}
