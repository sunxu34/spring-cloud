package com.sun.order.service.impl;

import com.sun.order.DTO.OrderDTO;
import com.sun.order.dataobject.OrderMaster;
import com.sun.order.enums.OrderStatusEnum;
import com.sun.order.enums.PayStatusEnum;
import com.sun.order.repository.OrderDetailRepository;
import com.sun.order.repository.OrderMasterRepository;
import com.sun.order.service.OrderService;
import com.sun.order.utils.KeyUtil;
import java.math.BigDecimal;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

  @Autowired
  private OrderMasterRepository orderMasterRepository;

  @Autowired
  private OrderDetailRepository orderDetailRepository;

  @Override
  public OrderDTO create(OrderDTO orderDTO) {

    // 步骤：
    //TODO 1、查询商品信息（调用商品服务）
    //TODO 2、计算总价
    //TODO 3、扣库存（调用商品服务）
    // 4、订单入库
    OrderMaster orderMaster = new OrderMaster();
    orderDTO.setOrderId(KeyUtil.genUniqueKey());
    BeanUtils.copyProperties(orderDTO, orderMaster);
    orderMaster.setOrderAmount(new BigDecimal(5));
    orderMaster.setOrderStatus(OrderStatusEnum.NEW.getCode());
    orderMaster.setPayStatus(PayStatusEnum.WAIT.getCode());

    orderMasterRepository.save(orderMaster);
    return orderDTO;
  }
}
