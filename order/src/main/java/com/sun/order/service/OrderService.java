package com.sun.order.service;

import com.sun.order.DTO.OrderDTO;

public interface OrderService {

  /**
   * 创建订单
   * @param orderDTO 入参实体
   * @return 返回实体
   */
  OrderDTO create(OrderDTO orderDTO);
}
