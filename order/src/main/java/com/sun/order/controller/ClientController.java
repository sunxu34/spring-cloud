package com.sun.order.controller;

import com.sun.order.client.ProductClient;
import com.sun.order.dataobject.ProductInfo;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
public class ClientController {

  // 第二种方式
//  @Autowired
//  private LoadBalancerClient loadBalancerClient;

  // 第三种方式
//  @Autowired
//  private RestTemplate restTemplate;

  @Autowired
  private ProductClient productClient;

  /**
   * 调用商品服务
   * @return 返回值
   */
  @GetMapping("/getProductMsg")
  public String getProductMsg(){

    // 第一种方式，直接使用RestTemplate（url写死）
//    RestTemplate restTemplate = new RestTemplate();
//    String response = restTemplate.getForObject("http://localhost:8861/msg", String.class);

    // 第二种方式，先使用LoadBalancerClient根据服务名称获取ip和端口
    // 再使用RestTemplate调用服务
//    ServiceInstance serviceInstance = loadBalancerClient.choose("PRODUCT");
//    String host = serviceInstance.getHost();
//    int port = serviceInstance.getPort();
//    String url = String.format("http://%s:%s", host, port) + "/msg";
//    RestTemplate restTemplate = new RestTemplate();
//    String response = restTemplate.getForObject(url, String.class);

    // 第三种方式，利用@LoadBalanced注解，可在RestTemplate里直接使用服务的名称
//    String response =
//        restTemplate.getForObject("http://PRODUCT/msg", String.class);

    String response = productClient.producMsg();
    log.info("response={}", response);
    return response;
  }

  @GetMapping("/getProductList")
  public String getProductList(){
    List<ProductInfo> productInfoList =
        productClient.listForOrder(Arrays.asList("157875196366160022"));
    log.info("response={}", productInfoList);
    return "ok";
  }

//  @GetMapping("/productDecreaseStock")
//  public
}
