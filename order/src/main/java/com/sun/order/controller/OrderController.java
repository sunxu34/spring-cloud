package com.sun.order.controller;

import com.sun.order.DTO.OrderDTO;
import com.sun.order.VO.ResultVO;
import com.sun.order.converter.OrderForm2OrderDTOConverter;
import com.sun.order.enums.ResultEnum;
import com.sun.order.exception.OrderException;
import com.sun.order.form.OrderForm;
import com.sun.order.service.OrderService;
import com.sun.order.utils.ResultVOUtil;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {

  @Autowired
  private OrderService orderService;

  /**
   * 步骤：
   *  1.参数校验
   *  2.查询商品信息（调用商品服务）
   *  3.计算总价
   *  4.扣库存（调用商品服务）
   *  5.订单入库
   *
   * @param orderForm 前端入参实体
   * @param bindingResult 参数校验结果
   * @return 返回值
   */
  @PostMapping("/create")
  public ResultVO<Map<String, String>> create(@Validated OrderForm orderForm, BindingResult bindingResult){
    // 参数校验
    if (bindingResult.hasErrors()) {
      log.error("【创建订单】 参数不正确， orderForm={}", orderForm);
      throw new OrderException(ResultEnum.PARAM_ERROR.getCode(), bindingResult.getFieldError().getDefaultMessage());
    }

    // orderForm 转换为 orderDTO
    OrderDTO orderDTO = OrderForm2OrderDTOConverter.convert(orderForm);
    if (CollectionUtils.isEmpty(orderDTO.getOrderDetailList())){
      log.error("【创建订单】 购物车信息为空");
      throw new OrderException(ResultEnum.CART_EMPTY);
    }

    OrderDTO result = orderService.create(orderDTO);

    Map<String, String> map = new HashMap<>();
    map.put("orderId", result.getOrderId());
    return ResultVOUtil.success(map);
  }


}
