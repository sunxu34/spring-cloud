package com.sun.order.client;

import com.sun.order.DTO.CartDTO;
import com.sun.order.dataobject.ProductInfo;
import java.util.List;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "product")
public interface ProductClient {

  @GetMapping("/msg")
  String producMsg();

  @PostMapping("/product/listForOrder")
  List<ProductInfo> listForOrder(@RequestBody List<String> productIdList);

  @PostMapping("/product/decreaseStock")
  void decreaseStock(@RequestBody List<CartDTO> cartDTOList);
}
