package com.sun.order.repository;

import com.sun.order.dataobject.OrderMaster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 订单持久层
 */
@Repository
public interface OrderMasterRepository extends JpaRepository<OrderMaster, String> {


}
