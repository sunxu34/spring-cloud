package com.sun.order.repository;

import com.sun.order.dataobject.OrderDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * 订单详情持久层
 */
@Repository
public interface OrderDetailRepository extends JpaRepository<OrderDetail, String> {

}
