package com.sun.order.enums;

import lombok.Getter;

/**
 * 订单状态枚举类
 */
@Getter
public enum OrderStatusEnum {

  NEW(0, "新订单"),
  FINISHED(1, "已完成"),
  CANCLE(2, "取消"),
  ;

  private Integer code;
  private String message;

  OrderStatusEnum(Integer code, String message) {
    this.code = code;
    this.message = message;
  }
}
