package com.sun.order.repository;


import com.sun.order.OrderApplicationTests;
import com.sun.order.dataobject.OrderMaster;
import com.sun.order.enums.OrderStatusEnum;
import com.sun.order.enums.PayStatusEnum;
import java.math.BigDecimal;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderMasterRepositoryTest extends OrderApplicationTests {

  @Autowired
  private OrderMasterRepository orderMasterRepository;

  @Test
  public void testSave(){
    OrderMaster orderMaster = new OrderMaster();
    orderMaster.setOrderId("1234567");
    orderMaster.setBuyerName("李万基");
    orderMaster.setBuyerPhone("13212341234");
    orderMaster.setBuyerAddress("印度");
    orderMaster.setBuyerOpenid("110119120");
    orderMaster.setOrderAmount(new BigDecimal(2.5));
    orderMaster.setOrderStatus(OrderStatusEnum.NEW.getCode());
    orderMaster.setPayStatus(PayStatusEnum.WAIT.getCode());

    OrderMaster result = orderMasterRepository.save(orderMaster);
    Assert.assertTrue(result != null);
  }
}